<?php
/*
 * Dfinimos la estructura de conexión a nuestra base de datos
 * Necesitamos los datos del archivo config.php
 * */

require 'config.php';

class Conexion{
    
    protected $conexion_db;
    
    // CONSTRUCTOR
    public function Conexion(){
        
        try {
            
            $this->conexion_db = new PDO(
                'mysql:host='. DB_HOST .'; dbname='. DB_NOMBRE .'', DB_USUARIO, DB_CONTRA
            );
            
            $this->conexion_db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            
            $this->conexion_db->exec("SET CHARACTER SET utf8");
            
            return $this->conexion_db;
            
        } catch (Exception $e){
            
            echo "Se ha producido un error durante la conexión: " . $e;
            
        }
        
        
    }
    
    
}

