<?php

// Capturamos la excepción que se genera en caso de error.
try{
    
    // Instanciamos la clase PDO, pasando los argumentos al constructor
    $base = new PDO('mysql:host=localhost; dbname=blog_udemy', 'root', '');
    
    $base->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    
    echo "Conexión realizada correctamente";
    
}catch(Exception $exep){
    
    die('Error ' . $exep->getMessage());
    
}finally {
    
    $base = NULL;
}