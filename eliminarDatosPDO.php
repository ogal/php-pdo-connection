<?php
/**
 * En este programa veremos un ejemplo de consulta preparada con PDO.
 * */

$busqueda_id = $_POST["id"];

// Capturamos la excepción que se genera en caso de error.
try{
    
    // Instanciamos la clase PDO, pasando los argumentos al constructor
    $base = new PDO('mysql:host=localhost; dbname=blog_udemy', 'root', '');
    
    $base->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    
    $base->exec("SET CHARACTER SET utf8");
    
    /* Utilizando marcadores en nuestra insercion, estos no tienen nada que 
     * ver con el nombre de la variable ni con el nombre del campo de la tabla.
    */ 
    $sql = "DELETE FROM posts WHERE id = :marcador_id";
    
    // Llamamos al metodo prepare del objeto conexion (base) y le pasamos la consulta,
    // Esto nos devuelve un objeto de tipo PDOStatement que lo almacenamos en resultado.
    $resultado = $base->prepare($sql);
    
    // 1. Ejecutamos el objeto de tipo PDOEstatement, y le pasamos el id que queramos
    $resultado->execute(array(":marcador_id" => $busqueda_id));
    
    echo "Registro eliminado con éxito";
    
    // 3. Cerramos el bufer del cursor
    $resultado->closeCursor();
    
}catch(Exception $exep){
    
    die('Error ' . $exep->getMessage());
    
}finally {
    
    $base = NULL;
}