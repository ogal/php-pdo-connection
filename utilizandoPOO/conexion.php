<?php
/*
 * Dfinimos la estructura de conexión a nuestra base de datos
 * Necesitamos los datos del archivo config.php
 * */

require 'config.php';

class Conexion{
    
    protected $conexion_db;
    
    // CONSTRUCTOR
    public function Conexion(){
        
        $this->conexion_db = new mysqli(DB_HOST, DB_USUARIO, DB_CONTRA, DB_NOMBRE);
        
        if ($this->conexion_db->connect_errno){
            
            echo "Fallo al conectar a MariaDB: " . $this->conexion_db->connect_error;
            
            return;
        }
        
        $this->conexion_db->set_charset(DB_CHARSET);
    }
    
    
}

